-- INSERT INTO DATASOURCECONFIG VALUES (1, 'org.h2.Driver', 'jdbc:h2:mem:secondDS', 'secondDS', 'sa', '', true);
-- INSERT INTO DATASOURCECONFIG VALUES (2, 'org.h2.Driver', 'jdbc:h2:mem:thirdDS', 'thirdDS', 'sa', '', true);
-- INSERT INTO DATASOURCECONFIG VALUES (3, 'org.h2.Driver', 'jdbc:h2:mem:fourDS', 'fourDS', 'sa', '', true);

CREATE TABLE "orders" (
"id" BIGSERIAL PRIMARY KEY,
"date" date NOT NULL
);


CREATE TABLE "products" (
"id" BIGSERIAL PRIMARY KEY,
"date" date NOT NULL, 
"product_name" VARCHAR(250) NOT NULL
);

CREATE TABLE "users" (
"id" BIGSERIAL PRIMARY KEY,
"date_created" date NOT NULL, 
"first_name" VARCHAR(200) NOT NULL, 
"last_name" VARCHAR(200) NOT NULL, 
"username" VARCHAR(250) NOT NULL, 
"password" VARCHAR(250) NOT NULL
);


INSERT INTO PRODUCT VALUES (1, 'Product 1');
INSERT INTO PRODUCT VALUES (2, 'Product 2');
INSERT INTO PRODUCT VALUES (3, 'Product 3');
INSERT INTO PRODUCT VALUES (4, 'Product 4');
INSERT INTO PRODUCT VALUES (5, 'Product 5');