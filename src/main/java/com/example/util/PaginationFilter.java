/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.util;

import java.util.Date;

/**
 *
 * @author Gidemn
 */
public class PaginationFilter {

    Integer pageNo;

    Integer pageSize;

    String sortBy;

    String search;

    String startDateString;

    String endDateString;

    Date startDate;

    Date endDate;

    SharedFunctions sharedF = new SharedFunctions();

    public PaginationFilter() {

    }

    public PaginationFilter(Integer pageNo, Integer pageSize, String sortBy, String search, String startDate, String endDate) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.sortBy = sortBy;
        this.search = search;

        if (startDate.trim().length() > 0) {
            this.startDate = sharedF.stringToDate(startDate);
        }

        if (endDate.trim().length() > 0) {
            this.endDate = sharedF.stringToDate(endDate);
        }
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
