/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.util;

/**
 *
 * @author Gidemn
 */
public class ApiReponse {

    private String error;

    public ApiReponse() {

    }

    public ApiReponse(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
