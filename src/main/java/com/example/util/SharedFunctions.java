/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gidemn
 */
public class SharedFunctions {

    public Date stringToDate(String dateToFormat) {
        String sDate1 = "1998-12-31";
        Date date = new Date();
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
        } catch (ParseException ex) {
            Logger.getLogger(SharedFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

}
