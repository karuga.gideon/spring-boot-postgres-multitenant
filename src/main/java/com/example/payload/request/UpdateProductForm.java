/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Gidemn
 */
public class UpdateProductForm {

    private String product_name;

    public UpdateProductForm(@JsonProperty("product_name") String product_name) {
        super();
        this.product_name = product_name;
    }

    public String getProductName() {
        return product_name;
    }

    public void setProductName(String product_name) {
        this.product_name = product_name;
    }

}
