/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.entity.User;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("SELECT u FROM User u WHERE u.dateCreated >= ?1 AND u.dateCreated <= ?2")
    public Page<User> dateFilterUsers(Date startDate, Date endDate, Pageable pageable);

    @Query("SELECT u FROM User u WHERE LOWER(u.username) = ?1")
    public Optional<User> findByUsername(String username);
    
    @Query("SELECT u FROM User u WHERE u.username = ?1 AND u.password = ?2")
    public Optional<User> findByUsernameAndPassword(String username, String password);

    @Query("SELECT u FROM User u WHERE (LOWER(u.firstName) LIKE %?1% OR LOWER(u.lastName) LIKE %?1% OR LOWER(u.username) LIKE %?1%)")
    public Page<User> searchUser(String search, Pageable pageable);

}
