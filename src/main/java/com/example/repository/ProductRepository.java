/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

/**
 *
 * @author Gidemn
 */
import org.springframework.stereotype.Repository;

import com.example.entity.Product;
import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {
    
    @Query("SELECT p FROM Product p WHERE LOWER(p.productName) LIKE %?1%")
    public Product findByProductName(String productName);

    @Query("SELECT p FROM Product p WHERE LOWER(p.productName) LIKE %?1%")
    public Page<Product> searchProduct(String keyword, Pageable pageable);

    @Query("SELECT p FROM Product p WHERE p.date >= ?1 AND p.date <= ?2")
    public Page<Product> dateFilterProducts(Date startDate, Date endDate, Pageable pageable);

}
