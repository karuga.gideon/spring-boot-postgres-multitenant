/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controller;

/**
 *
 * @author Gidemn
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.User;
import com.example.payload.request.CreateUserForm;
import com.example.repository.UserRepository;
import com.example.util.ApiReponse;
import com.example.util.PaginationFilter;
import java.util.Date;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@Transactional
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(path = "/users", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<User> createUser(
            @RequestBody CreateUserForm form) {

        User newUser = new User(new Date(), form.getFirstName(), form.getLastName(), form.getUsername(), form.getPassword());
        userRepository.save(newUser);

        return new ResponseEntity<>(newUser, HttpStatus.OK);
    }

    @RequestMapping(path = "/users", method = RequestMethod.GET, produces = "application/json")
    public Page<User> filterUsers(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "") String startDate,
            @RequestParam(defaultValue = "") String endDate) {

        PaginationFilter filter = new PaginationFilter(pageNo, pageSize, sortBy, search, startDate, endDate);
        Page<User> allUsers = sortUsers(filter);
        return allUsers;
    }

    public Page<User> sortUsers(PaginationFilter filter) {
        Page<User> allUsers;
        Pageable sortCriteria = PageRequest.of(filter.getPageNo(), filter.getPageSize(), Sort.by(filter.getSortBy()).descending());
        if (filter.getSearch().trim().length() > 0) {
            allUsers = userRepository.searchUser(filter.getSearch().toLowerCase(), sortCriteria);
        } else if (filter.getStartDate() != null && filter.getEndDate() != null) {
            allUsers = userRepository.dateFilterUsers(filter.getStartDate(), filter.getEndDate(), sortCriteria);
        } else {
            allUsers = userRepository.findAll(sortCriteria);
        }
        return allUsers;
    }

    @RequestMapping(path = "/user/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Object> getUser(@PathVariable("id") int id) {
        Optional<User> user = userRepository.findById(id);
        if (user.equals(Optional.empty())) {
            ApiReponse apiResponse = new ApiReponse("User not found!");
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

}
