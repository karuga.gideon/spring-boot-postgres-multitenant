/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controller;

/**
 *
 * @author Gidemn
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Product;
import com.example.payload.request.CreateProductForm;
import com.example.payload.request.UpdateProductForm;
import com.example.repository.ProductRepository;
import com.example.util.ApiReponse;
import com.example.util.PaginationFilter;

import java.sql.Date;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@Transactional
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(path = "/products", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Product> createProduct(
            @RequestBody CreateProductForm form) {

        Product newProduct = new Product(new Date(System.currentTimeMillis()), form.getProductName());
        productRepository.save(newProduct);
        return new ResponseEntity<>(newProduct, HttpStatus.OK);
    }

    @RequestMapping(path = "/products", method = RequestMethod.GET, produces = "application/json")
    public Page<Product> filterProducts(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "") String startDate,
            @RequestParam(defaultValue = "") String endDate) {

        PaginationFilter filter = new PaginationFilter(pageNo, pageSize, sortBy, search, startDate, endDate);
        Page<Product> allProducts = sortProducts(filter);
        return allProducts;
    }

    public Page<Product> sortProducts(PaginationFilter filter) {

        Page<Product> allProducts;
        Pageable sortCriteria = PageRequest.of(filter.getPageNo(), filter.getPageSize(), Sort.by(filter.getSortBy()).descending());
        if (filter.getSearch().trim().length() > 0) {
            allProducts = productRepository.searchProduct(filter.getSearch().toLowerCase(), sortCriteria);
        } else if (filter.getStartDate() != null && filter.getEndDate() != null) {
            allProducts = productRepository.dateFilterProducts(filter.getStartDate(), filter.getEndDate(), sortCriteria);
        } else {
            allProducts = productRepository.findAll(sortCriteria);
        }
        return allProducts;
    }

    @RequestMapping(path = "/product/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Object> getProduct(@PathVariable("id") int id) {

        Optional<Product> product = productRepository.findById(id);
        if (product.equals(Optional.empty())) {
            ApiReponse apiResponse = new ApiReponse("Product not found!");
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
    }

    @RequestMapping(path = "/product/{id}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateProduct(
            @PathVariable("id") int id,
            @RequestBody UpdateProductForm form) {

        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.equals(Optional.empty())) {
            ApiReponse apiResponse = new ApiReponse("Product not found!");
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        } else {

            Product product = optionalProduct.get();
            product.setProductName(form.getProductName());
            productRepository.save(product);

            return new ResponseEntity<>(product, HttpStatus.OK);
        }
    }

}
