package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Order;
import com.example.repository.OrderRepository;
import com.example.util.ApiReponse;
import com.example.util.PaginationFilter;

import java.sql.Date;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@Transactional
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @RequestMapping(path = "/orders", method = RequestMethod.POST)
    public ResponseEntity<?> createSampleOrder() {

        Order newOrder = new Order(new Date(System.currentTimeMillis()));
        orderRepository.save(newOrder);
        return ResponseEntity.ok(newOrder);

    }

    @RequestMapping(path = "/orders", method = RequestMethod.GET)
    public Page<Order> filterOrders(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "") String startDate,
            @RequestParam(defaultValue = "") String endDate) {

        PaginationFilter filter = new PaginationFilter(pageNo, pageSize, sortBy, search, startDate, endDate);
        Page<Order> allOrders = sortOrders(filter);
        return allOrders;
    }

    public Page<Order> sortOrders(PaginationFilter filter) {
        Page<Order> allOrders;
        Pageable sortCriteria = PageRequest.of(filter.getPageNo(), filter.getPageSize(), Sort.by(filter.getSortBy()).descending());
        if (filter.getStartDate() != null && filter.getEndDate() != null) {
            allOrders = orderRepository.dateFilterOrders(filter.getStartDate(), filter.getEndDate(), sortCriteria);
        } else {
            allOrders = orderRepository.findAll(sortCriteria);
        }
        return allOrders;
    }

    @RequestMapping(path = "/order/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Object> getProduct(@PathVariable("id") int id) {
        Optional<Order> order = orderRepository.findById(id);
        if (order.equals(Optional.empty())) {
            ApiReponse apiResponse = new ApiReponse("Order not found!");
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(order, HttpStatus.OK);
        }
    }

}
