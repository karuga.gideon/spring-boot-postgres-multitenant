/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test;

import com.example.entity.Product;
import com.example.repository.ProductRepository;
import com.example.util.PaginationFilter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Gidemn
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void testSaveNewProduct() {
        Product saveProduct = entityManager.persist(new Product(new Date(System.currentTimeMillis()), "Android"));
        assertThat(saveProduct.getId()).isGreaterThan(0);
    }

    @Test
    public void findPrdocuctByID() {
        Product otherProduct = entityManager.persist(new Product(new Date(System.currentTimeMillis()), "iPhone 10"));
        assertThat(otherProduct.getId()).isGreaterThan(0);

        Optional<Product> findProduct = productRepository.findById(otherProduct.getId());
        assertThat(findProduct.equals(Optional.empty())).isEqualTo(false);
    }

    @Test
    public void testFilterProducts() {

        Product product1 = entityManager.persist(new Product(new Date(System.currentTimeMillis()), "Book"));
        assertThat(product1.getId()).isGreaterThan(0);

        Product product2 = entityManager.persist(new Product(new Date(System.currentTimeMillis()), "Pen"));
        assertThat(product2.getId()).isGreaterThan(0);

        PaginationFilter filter = new PaginationFilter(0, 10, "id", "", "", "");
        Pageable sortCriteria = PageRequest.of(filter.getPageNo(), filter.getPageSize(), Sort.by(filter.getSortBy()).descending());
        Page<Product> allProducts = productRepository.findAll(sortCriteria);
        assertThat(allProducts.getTotalElements()).isEqualTo(2);

        Iterable<Product> productsIterator = productRepository.findAll();
        List<Product> productList = new ArrayList<>();
        productsIterator.iterator().forEachRemaining(productList::add);
        assertThat(productList.size()).isEqualTo(2);
    }
}
