/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test;

import com.example.entity.Order;
import com.example.repository.OrderRepository;
import com.example.util.PaginationFilter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;


/**
 *
 * @author Gidemn
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void testSaveNewOrder() {
        Order saveOrder = entityManager.persist(new Order(new Date(System.currentTimeMillis())));
        assertThat(saveOrder.getId()).isGreaterThan(0);
    }

    @Test
    public void findOrderByID() {
        Order otherOrder = entityManager.persist(new Order(new Date(System.currentTimeMillis())));
        assertThat(otherOrder.getId()).isGreaterThan(0);

        Optional<Order> findOrder = orderRepository.findById(otherOrder.getId());
        assertThat(findOrder.equals(Optional.empty())).isEqualTo(false);
    }

    @Test
    public void testFilterOrders() {

        Order order1 = entityManager.persist(new Order(new Date(System.currentTimeMillis())));
        assertThat(order1.getId()).isGreaterThan(0);

        Order order2 = entityManager.persist(new Order(new Date(System.currentTimeMillis())));
        assertThat(order2.getId()).isGreaterThan(0);

        PaginationFilter filter = new PaginationFilter(0, 10, "id", "", "", "");
        Pageable sortCriteria = PageRequest.of(filter.getPageNo(), filter.getPageSize(), Sort.by(filter.getSortBy()).descending());
        Page<Order> allOrders = orderRepository.findAll(sortCriteria);
        assertThat(allOrders.getTotalElements()).isEqualTo(2);

        Iterable<Order> productsIterator = orderRepository.findAll();
        List<Order> productList = new ArrayList<>();
        productsIterator.iterator().forEachRemaining(productList::add);
        assertThat(productList.size()).isEqualTo(2);
    }
}
