/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test;

import com.example.entity.User;
import com.example.repository.UserRepository;
import com.example.util.PaginationFilter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Gidemn
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testSaveNewUser() {

        User newUser = new User();
        newUser.setDateCreated(new java.util.Date());
        newUser.setFirstName("John");
        newUser.setLastName("Doe");
        newUser.setUsername("john_doe");
        newUser.setPassword("123456");

        User saveUser = entityManager.persist(newUser);
        assertThat(saveUser.getId()).isGreaterThan(0);
    }

    @Test
    public void findUserByID() {
        User newUser = new User();
        newUser.setDateCreated(new java.util.Date());
        newUser.setFirstName("John");
        newUser.setLastName("Doe");
        newUser.setUsername("john_doe");
        newUser.setPassword("123456");

        User saveUser = entityManager.persist(newUser);
        assertThat(saveUser.getId()).isGreaterThan(0);

        Optional<User> findUser = userRepository.findById(newUser.getId());
        assertThat(findUser.equals(Optional.empty())).isEqualTo(false);
    }

    @Test
    public void findUserByUsername() {
        User newUser = new User();
        newUser.setDateCreated(new java.util.Date());
        newUser.setFirstName("John");
        newUser.setLastName("Doe");
        newUser.setUsername("john_doe");
        newUser.setPassword("123456");

        User saveUser = entityManager.persist(newUser);
        assertThat(saveUser.getId()).isGreaterThan(0);

        Optional<User> findUser = userRepository.findByUsername("john_doe");
        assertThat(findUser.equals(Optional.empty())).isEqualTo(false);
    }

    @Test
    public void testFilterUsers() {

        User user1 = new User();
        user1.setDateCreated(new java.util.Date());
        user1.setFirstName("John");
        user1.setLastName("Doe");
        user1.setUsername("john_doe");
        user1.setPassword("123456");

        User saveUser1 = entityManager.persist(user1);
        assertThat(saveUser1.getId()).isGreaterThan(0);

        User user2 = new User();
        user2.setDateCreated(new java.util.Date());
        user2.setFirstName("Jane");
        user2.setLastName("Doe");
        user2.setUsername("jane_doe");
        user2.setPassword("12345678");

        User saveUser2 = entityManager.persist(user2);
        assertThat(saveUser2.getId()).isGreaterThan(0);

        PaginationFilter filter = new PaginationFilter(0, 10, "id", "", "", "");
        Pageable sortCriteria = PageRequest.of(filter.getPageNo(), filter.getPageSize(), Sort.by(filter.getSortBy()).descending());
        Page<User> allUsers = userRepository.findAll(sortCriteria);
        assertThat(allUsers.getTotalElements()).isEqualTo(2);

        Iterable<User> usersIterator = userRepository.findAll();
        List<User> userList = new ArrayList<>();
        usersIterator.iterator().forEachRemaining(userList::add);
        assertThat(userList.size()).isEqualTo(2);
    }
}
